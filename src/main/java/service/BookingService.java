package service;

import model.Booking;
import model.Room;
import model.User;

import java.time.LocalDate;
import java.util.*;

public class BookingService {
    private static Long nextId = 0L;
    private List<Room> rooms = new ArrayList<>();
    private List<User> users = new ArrayList<>();
    private Map<Long, Booking> bookings = new HashMap<>();

    public BookingService() {
        fillRooms();
        fillUsers();
    }

    public Long bookRoom(final User user, final Room room, final LocalDate startDate, final int periodOfTime) {
        if (!isRoomAvailable(room, startDate, periodOfTime))
            throw new IllegalArgumentException("Room is not available for reservation for the given time period");

        removeReservedDaysFromRoom(room, startDate, periodOfTime);
        bookings.put(nextId++, new Booking(room, user, startDate, periodOfTime));
        return nextId - 1;
    }

    public void cancelRoom(final User user, Long bookingId) {
        var booking = bookings.get(bookingId);
        if (booking == null)
            return;
        if (!booking.getUser().equals(user))
            throw new IllegalArgumentException("You can not cancel booking with id:" + bookingId + " because it was not you who made the reservation.");
        bookings.remove(bookingId);
        freeReservedDaysForRoom(booking.getBookedRoom(), booking.getDate(), booking.getPeriodOfTime());
        System.out.println("Room cancelled successfully with id: " + bookingId);
    }

    public List<Booking> findAllBookingsForUser(final User user) {
        return bookings.values().stream()
                .filter(booking -> booking.getUser().equals(user))
                .toList();
    }

    public List<Booking> findAllBookingsForUserWithFilters(final User user, final LocalDate filterDate, final Double filterPrice) {
        var filteredList = new ArrayList<Booking>();
        for (var userBooking : findAllBookingsForUser(user)) {
            boolean dateFilter = true;
            boolean priceFilter = true;
            if (filterDate != null && (userBooking.getDate().minusDays(1).isAfter(filterDate) || userBooking.getDate().plusDays(userBooking.getPeriodOfTime()).isBefore(filterDate)))
                dateFilter = false;
            if (filterPrice != null && userBooking.getBookedRoom().getPrice() != filterPrice)
                priceFilter = false;

            if (dateFilter && priceFilter)
                filteredList.add(userBooking);
        }
        return filteredList;
    }

    public void printUserHistory(final User user) {
        var list = findAllBookingsForUser(user);
        System.out.println("User history = " + list);
    }

    private boolean checkUser(final User user, Long bookingId) {
        var booking = bookings.get(bookingId);
        if (booking == null)
            return false;
        return booking.getUser().equals(user);
    }

    private boolean isRoomAvailable(final Room room, final LocalDate startDate, final int periodOfTime) {
        Set<LocalDate> dateRangeInInterest = makeDateSet(startDate, periodOfTime);
        return room.getAvailableDates().containsAll(dateRangeInInterest);
    }

    private boolean removeReservedDaysFromRoom(final Room room, final LocalDate startDate, final int periodOfTime) {
        Set<LocalDate> dateRangeInInterest = makeDateSet(startDate, periodOfTime);
        return room.getAvailableDates().removeAll(dateRangeInInterest);
    }

    private boolean freeReservedDaysForRoom(final Room room, final LocalDate startDate, final int periodOfTime) {
        Set<LocalDate> dateRangeInInterest = makeDateSet(startDate, periodOfTime);
        return room.getAvailableDates().addAll(dateRangeInInterest);
    }

    private Set<LocalDate> makeDateSet(final LocalDate startDate, final int periodOfTime) {
        Set<LocalDate> dateList = new LinkedHashSet<>();
        for (int i = 0; i < periodOfTime; i++) {
            dateList.add(startDate.plusDays(i));
        }
        return dateList;
    }

    private Set<LocalDate> getDates() {
        var now = LocalDate.now();
        var dates = new LinkedHashSet<LocalDate>();
        for (int i = 0; i < 60; i++) {
            dates.add(now.plusDays(i));
        }
        return dates;
    }

    private void fillRooms() {
        for (int i = 0; i < 30; i++) {
            rooms.add(new Room(nextId++, "room" + i, i * 1000, getDates()));
        }
    }

    private void fillUsers() {
        for (int i = 0; i < 30; i++) {
            users.add(new User(nextId++, "user" + i));
        }
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public Map<Long, Booking> getBookings() {
        return bookings;
    }

    public void setBookings(Map<Long, Booking> bookings) {
        this.bookings = bookings;
    }
}
