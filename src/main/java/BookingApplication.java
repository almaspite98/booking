import service.BookingService;

import java.time.LocalDate;

public class BookingApplication {

    public static void main(String[] args) {
        BookingService bookingService = new BookingService();
        var user = bookingService.getUsers().get(5);
        bookingService.bookRoom(user, bookingService.getRooms().get(0), LocalDate.now().plusDays(10), 5);
        bookingService.bookRoom(user, bookingService.getRooms().get(10), LocalDate.now().plusDays(10), 5);
        bookingService.bookRoom(user, bookingService.getRooms().get(20), LocalDate.now().plusDays(10), 5);
        var id = bookingService.bookRoom(user, bookingService.getRooms().get(25), LocalDate.now().plusDays(10), 5);
        bookingService.printUserHistory(user);
        bookingService.cancelRoom(user, id);
        bookingService.printUserHistory(user);
        var filtered1 = bookingService.findAllBookingsForUserWithFilters(user, null, null);
        for (var booking : filtered1)
            System.out.println("booking1 = " + booking);
        var filtered2 = bookingService.findAllBookingsForUserWithFilters(user, null, 10000.0);
        for (var booking : filtered2)
            System.out.println("booking2 = " + booking);
        var filtered3 = bookingService.findAllBookingsForUserWithFilters(user, LocalDate.now().plusDays(12), null);
        for (var booking : filtered3)
            System.out.println("booking3 = " + booking);
        var filtered4 = bookingService.findAllBookingsForUserWithFilters(user, LocalDate.now().plusDays(12), 10000.0);
        for (var booking : filtered4)
            System.out.println("booking4 = " + booking);
    }
}
