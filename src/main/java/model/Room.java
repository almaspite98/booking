package model;



import java.time.LocalDate;
import java.util.LinkedHashSet;
import java.util.Set;

public class Room {
    private Long id;
    private String name;
    private double price;
    Set<LocalDate> availableDates = new LinkedHashSet<>();

    public Room(Long id, String name, double price, Set<LocalDate> availableDates) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.availableDates = availableDates;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Set<LocalDate> getAvailableDates() {
        return availableDates;
    }

    public void setAvailableDates(Set<LocalDate> availableDates) {
        this.availableDates = availableDates;
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
