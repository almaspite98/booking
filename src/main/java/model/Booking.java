package model;



import java.time.LocalDate;

public class Booking {
    private Room bookedRoom;
    private User user;
    private LocalDate date;
    private int periodOfTime;

    public Booking(Room bookedRoom, User user, LocalDate date, int periodOfTime) {
        this.bookedRoom = bookedRoom;
        this.user = user;
        this.date = date;
        this.periodOfTime = periodOfTime;
    }

    public Room getBookedRoom() {
        return bookedRoom;
    }

    public void setBookedRoom(Room bookedRoom) {
        this.bookedRoom = bookedRoom;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getPeriodOfTime() {
        return periodOfTime;
    }

    public void setPeriodOfTime(int periodOfTime) {
        this.periodOfTime = periodOfTime;
    }

    @Override
    public String toString() {
        return "Booking{" +
                "bookedRoom=" + bookedRoom +
                ", user=" + user +
                ", date=" + date +
                ", periodOfTime=" + periodOfTime +
                '}';
    }

}
